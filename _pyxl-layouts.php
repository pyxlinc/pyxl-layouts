<?php
/*
 * Plugin Name: (custom) Layouts
 * Description: ACF flexible content implementation with dynamically declared page templates.
 * Version: 1.1.0
 * Author: Pyxl Inc.
 * Author URI: http://pyxl.com
 * License: GPL
 */

namespace Pyxl\Layouts;

defined('WPINC') || die;

define(__NAMESPACE__.'\PATH', plugin_dir_path(__FILE__));
define(__NAMESPACE__.'\URI', plugin_dir_url(__FILE__));

require_once 'lib/autoload.php';
