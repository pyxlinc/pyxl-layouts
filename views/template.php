<?php

use Pyxl\Layouts\Render;

get_header();

while (have_posts()) :
    the_post();

    Render::template();

endwhile;

get_footer();
