<?php

namespace Pyxl\Layouts;

class RegisterFieldGroup
{
    public $field_group;

    public function __construct($template)
    {
        $this->field_group = [
            'key'                   => 'group_' . $template['slug'],
            'title'                 => $template['name'],
            'menu_order'            => 0,
            'position'              => 'normal',
            'style'                 => 'default',
            'label_placement'       => 'top',
            'instruction_placement' => 'label',
            'description'           => '',
            'fields'                => [
                [
                    'key'          => $template['path'] . 'views/layouts/' . $template['slug'],
                    'name'         => 'layouts/' . $template['slug'],
                    'type'         => 'flexible_content',
                    'label'        => '',
                    'button_label' => 'Add Module',
                    'layouts'      => apply_filters('pyxl/layouts/' . $template['slug'], []),
                ],
            ],
            'hide_on_screen'        => [
                0 => 'the_content',
                1 => 'excerpt',
                2 => 'custom_fields',
                3 => 'featured_image',
            ],
            'location'              => apply_filters(
                'pyxl/layouts/locations/' . $template['slug'],
                [
                    [
                        [
                            'param'    => 'post_template',
                            'operator' => '==',
                            'value'    => $template['virtual_file'],
                        ],
                    ],
                ]
            ),
        ];

        add_action('wp_loaded', [$this, 'register'], 100);
    }

    public function register()
    {
        if (!function_exists('acf_add_local_field_group')) {
            return -1;
        }
        acf_add_local_field_group($this->field_group);
    }
}
