<?php

namespace Pyxl\Layouts;

/**
 * Class Register
 * @package Pyxl\Layouts
 */
class Register
{
    public $template;

    public static function init($template)
    {
        $template['slug']         = array_key_exists('slug', $template) ? $template['slug'] :
            preg_replace('/\s+/', '_', strtolower($template['name']));
        $template['virtual_file'] = $template['slug'];

        new RegisterPageTemplate($template);
        new RegisterFields($template);
        new RegisterFieldGroup($template);
    }
}
