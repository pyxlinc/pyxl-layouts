<?php

// https://wordpress.stackexchange.com/questions/3396/create-custom-page-templates-with-plugins
// https://github.com/wpexplorer/page-templater/blob/master/pagetemplater.php

namespace Pyxl\Layouts;

use Timber\Timber;

class RegisterPageTemplate
{
    public $template;

    public $template_registry;

    public function __construct($template)
    {
        $this->template = $template;

        $this->template_registry = [
            $template['virtual_file'] => $template['name'],
        ];

        // Add a filter to the save post to inject out template into the page cache
        add_filter('wp_insert_post_data', [$this, 'registerProjectTemplates']);

        // Add a filter to the wp 4.7 version attributes metabox
        add_filter('theme_page_templates', [$this, 'addNewTemplate']);
        add_filter('theme_post_templates', [$this, 'addNewTemplate']);

        // Add a filter to the template include to determine if the page has our template assigned and return it's path
        add_filter('template_include', [$this, 'viewProjectTemplate']);
    }

    /**
     * Adds our template to the page Template select for v4.7+
     *
     * @param $posts_templates
     * @return array
     */
    public function addNewTemplate($posts_templates)
    {
        return array_merge($posts_templates, $this->template_registry);
    }

    /**
     * into thinking the template file exists where it doesn't really exist.
     * Adds our template to the pages cache in order to trick WordPress
     *
     * @param $atts
     * @return mixed
     */
    public function registerProjectTemplates($atts)
    {

        // Create the key used for the themes cache
        $cache_key = 'page_templates-'.md5(get_theme_root().'/'.get_stylesheet());

        // Retrieve the cache list.
        // If it doesn't exist, or it's empty prepare an array
        $templates = wp_get_theme()->get_page_templates();
        if (empty($templates)) {
            $templates = [];
        }

        // New cache, therefore remove the old one
        wp_cache_delete($cache_key, 'themes');

        // Now add our template to the list of templates by merging our templates
        // with the existing templates array from the cache.
        $templates = array_merge($templates, $this->template_registry);

        // Add the modified cache to allow WordPress to pick it up for listing
        // available templates
        wp_cache_add($cache_key, $templates, 'themes', 1800);

        return $atts;
    }

    /**
     * Checks if the template is assigned to the page
     *
     * @param $template
     * @return string
     */
    public function viewProjectTemplate($template)
    {

        // Return the search template if we're searching (instead of the template for the first result)
        if (is_search()) {
            return $template;
        }

        // Get global post
        global $post;

        // Return template if post is empty
        if (!$post) {
            return $template;
        }

        // todo: Evaluate this if statement
        // Return default template if we don't have a custom one defined
        $page_template_slug = get_post_meta($post->ID, '_wp_page_template', true);

        if (!array_key_exists($page_template_slug, $this->template_registry)) {
            return $template;
        }
        $path = PATH.'views/template.php';

        // Return template
        return $path;
    }
}