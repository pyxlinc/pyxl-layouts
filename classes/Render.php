<?php

namespace Pyxl\Layouts;

use Timber\Timber;

class Render
{
    public static function template()
    {
        if (!function_exists('get_field')) {
            return;
        }

        $post_id = get_the_ID();

        if (!$post_id) {
            return;
        }

        $template_file = get_post_meta($post_id, '_wp_page_template', true);
        $layout_path   = "layouts/{$template_file}";
        $layouts       = get_field($layout_path, $post_id);
        $layout_object = get_field_object($layout_path, $post_id);
        $path_plugin   = $layout_object['key'];
        $path_theme    = get_stylesheet_directory();

        if (!$layouts) {
            return;
        }

        foreach ($layouts as $layout_meta) {
            if (!array_key_exists('acf_fc_layout', $layout_meta)) {
                continue;
            }

            $layout = preg_replace('/[_]+/', '-', $layout_meta['acf_fc_layout']);

            $path_include = '';
            $file_type    = '';

            $layout_theme_twig = "{$path_theme}/views/{$layout_path}/{$layout}/{$layout}.twig";
            $layout_theme_php  = "{$path_theme}/views/{$layout_path}/{$layout}/{$layout}.php";

            $layout_plugin_twig = "{$path_plugin}/{$layout}/{$layout}.twig";
            $layout_plugin_php  = "{$path_plugin}/{$layout}/{$layout}.php";

            if (file_exists($layout_plugin_php)) {
                $path_include = $layout_plugin_php;
                $file_type    = 'php';
            };

            if (file_exists($layout_plugin_twig)) {
                $path_include = $layout_plugin_twig;
                $file_type    = 'twig';
            };

            if (file_exists($layout_theme_php)) {
                $path_include = $layout_theme_php;
                $file_type    = 'php';
            };

            if (file_exists($layout_theme_twig)) {
                $path_include = $layout_theme_twig;
                $file_type    = 'twig';
            };

            if ('twig' === $file_type && class_exists(Timber::class)) {
                Timber::render($path_include, [
                    'context' => apply_filters("{$layout_path}/{$layout}", $layout_meta),
                ]);
            }

            if ('php' === $file_type) {
                include $path_include;
            }
        }
    }
}
