<?php

namespace Pyxl\Layouts;

class RegisterFields
{
    public $registry;

    public function __construct($template)
    {
        $this->registry = $template['layouts'];

        add_filter('pyxl/layouts/'.$template['slug'], [$this, 'layouts']);
    }

    public function layouts($layouts)
    {
        foreach ($this->registry as $layout) {
            $layouts[$layout->key] = $layout->layout;
        }

        return $layouts;
    }
}