<?php

namespace Pyxl\Layouts;

use ReflectionClass;

/**
 * Class Model
 * @package Pyxl\Layouts
 */
class Model
{
    public $layout;

    public $name;

    public $key;

    public $label;

    public $sub_fields;

    public $child_class;

    public function __construct($child)
    {
        $this->child_class = new ReflectionClass($child);

        $this->name = empty($child->name)
            ? $this->child_class->getShortName()
            : $child->name;

        $this->key = empty($child->key)
            ? strtolower(preg_replace('/\B([A-Z])/', '_$1', $this->name))
            : $child->key;

        $this->label = empty($child->label)
            ? preg_replace('/\B([A-Z])/', ' $1', $this->name)
            : $child->label;

        $this->layout = [
            'key'        => $this->key,
            'name'       => $this->key,
            'label'      => $this->label,
            'display'    => property_exists($child, 'display') ? $child->display : 'block',
            'sub_fields' => $this->keyFixer($child->sub_fields),
        ];
    }

    public function keyFixer($sub_fields, $parent = null)
    {
        $sub_fields_new = [];
        $i              = 0;

        foreach ($sub_fields as $value) {
            $sub_fields_new[]           = $value;
            $slug                       = array_key_exists('slug', $value) ? $value['slug'] : strtolower(preg_replace('/\B([A-Z])/', '_$1', $value['label']));
            $key = $this->generateKey($value, $parent);
            $sub_fields_new[$i]['key']  = $key;
            $sub_fields_new[$i]['name'] = array_key_exists('slug', $value) ? $value['slug'] : $value['name'];

            if (array_key_exists('sub_fields', $value)) {
                $sub_fields_new[$i]['sub_fields'] = $this->keyFixer($value['sub_fields'], $slug);
            }

            if (array_key_exists('conditional_logic', $value)) {
                $conditions = [];

                foreach($value['conditional_logic'][0] as $conditionIndex => $condition) {
                    $field = $condition['field'];
                    $fieldArray = explode('/', $field);
                    $conditionParent = count($fieldArray) === 2
                        ?   $fieldArray[0]
                        :   null;
                    $conditionSlug = count($fieldArray) === 2
                        ?   $fieldArray[1]
                        :   $fieldArray[0];

                    if ($conditionParent) {
                        foreach($this->sub_fields as $subField) {
                            $slug = array_key_exists('slug', $subField) ? $subField['slug'] : strtolower(preg_replace('/\B([A-Z])/', '_$1', $subField['label']));

                            if ($slug === $conditionParent) {
                                $subFields = $subField['sub_fields'];
                            }
                        }
                    } else {
                        $subFields = $this->sub_fields;
                    }
                    
                    foreach($subFields as $subField) {
                        $slug = array_key_exists('slug', $subField) ? $subField['slug'] : strtolower(preg_replace('/\B([A-Z])/', '_$1', $subField['label']));

                        if ($slug === $conditionSlug) {
                            $updatedConditional = array_merge(
                                $condition,
                                [
                                    'field' => $this->generateKey($subField, $conditionParent),
                                ]
                            );

                            $conditions[] = $updatedConditional;
                        }
                    }
                }

                $sub_fields_new[$i]['conditional_logic'] = $conditions;
            }

            $i++;
        }

        return $sub_fields_new;
    }

    public function generateKey($value, $parent = null) {
        $namespace                  = $this->child_class->name.'_';
        $cleaned_namespace          = preg_replace("/[\W]+/", "-", $namespace);
        $parent_key                 = $parent ? $parent.'_' : null;
        $slug                       = array_key_exists('slug', $value) ? $value['slug'] : strtolower(preg_replace('/\B([A-Z])/', '_$1', $value['label']));
        $key                        = $cleaned_namespace.$parent_key.$slug;

        return $key;
    }
}