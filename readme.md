# Pyxl Layouts

The Pyxl Layouts plugin is a helper library for building ACF field groups in an MVC format.

## Importing Layouts Library
In order to use the Layouts library, there needs to be a `use` statement at the top of the file referencing the library.

`use Pyxl\Layouts;`

## Usage
Once the library is imported, you can begin building your ACF field groups.  To get started, create a hook that gets called on `plugins_loaded`.  Inside this hook, there are a few functions to call to create your field group.

`Layouts\Register::init`

This function will register a field group and a page template for that field group.  The function takes a config for the field group you would like to register.

Config Array Structure:
``` php
[
      // name - This name will be used for the field group and page template
      'name'    => 'Modules',

      // slug - The slug needs to be unique and should be similar to the name
      'slug'    => 'modules',

      // path - This should always be the PATH variable
      'path'    => PATH,

      // layouts - An array of all Layouts to include in the field group
      'layouts' => [

          // Each entry should be a new instance of the Model class
          new Models\Layouts\HeroSimple(),
      ],
  ]
```

After registering the new field group, the controller for each layout needs to be initialized.

In the same hook called on `plugins_loaded` initialize each layout being used in the field group.

```php
Controllers\Layouts\HeroSimple::init();
```

## Models
Models are in charge of how the data will be structured when viewing in the admin section as well as the structure for `$context` in the controller

```php
// namespace should match the plugin namespace
namespace Pyxl\CRS\Models\Layouts;

// import the Model class from the layout plugin
use Pyxl\Layouts\Model;

// layout model should extend layout model
class HeroSimple extends Model
{
    // register the layout sub fields in the class constructor
    public function __construct()
    {
        // $this->sub_fields - should be an array of acf fields
        $this->sub_fields = [
            [
              [
                  'label' => 'Title',
                  'slug' => 'title',
                  'type'  => 'text',
              ],
            ],
        ];

        // Call the parent class's constructor passing in our layout model
        parent::__construct($this);
    }
}
```

### Conditional Logic
Each field has the option to only display if a condition is met.  The layouts library offers a way to alias the field being referenced for the conditional logic instead of having to infer the generated key.

This alias structure is `{parent_slug}/{field_slug}` or `{field_slug}` if no parent field is needed.

Example:
```php
[
    // basic field registration
    'label' => 'Sub Title',
    'slug' => 'sub_title',
    'type'  => 'text',

    // conditional logic key
    'conditional_logic' => [
      [
        [
          // field to check against
          'field' => 'title',

          // only display sub title field if title is equal to 'home'
          'operator' => '==',
          'value' => 'home'
        ]
      ]
    ]
],
```

## Controllers
Controllers are where any filtering for `$context` will happen right before it's passed to the view

```php
// namespace should match the plugin namespace
namespace Pyxl\CRS\Controllers\Layouts;

// create a new class for our layout controller
class HeroSimple
{

    // Create a public static function for initializing the controller
    public static function init()
    {
      // setup hook for filtering the controller context passed to  our view
      $class = new self;

      // filter structure should be layouts/{field_group_slug}/{hyphenated-model-name}
      add_filter('layouts/modules/hero-simple', [$class, 'filter']);
    }

    // create a function for handling the filtering of the view context
    public function filter($context)
    {
      // $context contains all data associated with the layout

      // Do some stuff here to massage the $context before it's sent to the view

      // return the edited $context to our view
      return $context;
    }
}
```

## Views
Views are where the data is actually displayed to the user.  Views can be either a `twig` template or a `php` template.

Twig Example:
All layout data is stored in the `context` variable.

```twig
{# 
  context.title = string
#}

<section>
  <h1>{{ context.title }}</h1>
</section>
```

PHP Example:
All layout data is stored in the `$data` variable.

```php
// $data['title'] = string

<section>
  <h1><?php echo $data['title']; ?></h1>
</section>
```
